FROM python:3

ARG PORT
ENV PORT $PORT

ADD main.py /
ADD requirements.txt /
ADD proto/converter.proto /
RUN mkdir songs

RUN pip install --no-cache-dir -r requirements.txt
RUN python -m grpc_tools.protoc -I . --python_out=. --grpc_python_out=. converter.proto
RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install ffmpeg -y

EXPOSE $PORT
CMD ["python", "./main.py"]