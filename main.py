import os
import grpc
import ffmpeg
import converter_pb2
import converter_pb2_grpc
from concurrent import futures


class ConverterService(converter_pb2_grpc.ConverterServicer):
    def ConvertSong(self, request: converter_pb2.ConversionRequest, context):
        quality = request.quality
        if quality == converter_pb2.Quality64:
            ab = '64k'
        elif quality == converter_pb2.Quality128:
            ab = '128k'
        elif quality == converter_pb2.Quality256:
            ab = '256k'
        elif quality == converter_pb2.Quality320:
            ab = '320k'
        else:
            ab = '320k'

        process = (
            ffmpeg
            .input('pipe:', format=request.extension)
            .output(f'songs/{request.songId}.mp3', format='mp3', ab=ab)
            .overwrite_output()
            .run_async(pipe_stdin=True)
        )

        process.communicate(input=request.songData)

        with open(f'songs/{request.songId}.mp3', 'rb') as f:
            song = f.read()
            f.close()
            os.remove(f'songs/{request.songId}.mp3')

            return converter_pb2.ConversionResponse(
                songId=request.songId,
                quality=request.quality,
                songData=song
            )


def main():
    port = os.getenv('PORT', 5002)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), options=[
        ('grpc.max_send_message_length', 500 * 1024 * 1024),
        ('grpc.max_receive_message_length', 500 * 1024 * 1024)
    ])
    converter_pb2_grpc.add_ConverterServicer_to_server(ConverterService(), server)
    print(f'Starting server. Listening on port {port}.')
    server.add_insecure_port(f'[::]:{port}')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    main()
